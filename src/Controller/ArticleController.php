<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use App\Form\ArticleType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;

class ArticleController extends AbstractController
{
    // /**
    //  * @Route("/article", name="article")
    //  */
    // public function index()
    // {
    //     return $this->render('article/index.html.twig', [
    //         'controller_name' => 'ArticleController',
    //     ]);
    // }

    /**
     * @Route ("/article/{id}", name="article")
     */
    function showArticle(Article $article)
    {
        return $this->render("home/article.html.twig", [
            "article" => $article
        ]);
    }

    /**
     * @Route("/remove/{article}", name="remove_article")   
     */
    public function remove(Article $article, ObjectManager $manager) {
        $manager->remove($article);
        $manager->flush();

        return $this->redirectToRoute("home");
    }

    /**
     * @Route("modify/{article}", name="modify_article")   
     */
    public function modify(Article $article, ObjectManager $manager, Request $request) {
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($article);
            $manager->flush();
            return $this->redirectToRoute("home");
        }

        return $this->render("article/index.html.twig", [
            "form" => $form->createView()
        ]);
    }
}
