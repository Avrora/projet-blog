<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ArticleRepository;
use App\Form\ArticleType;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Article;
use Doctrine\ORM\Mapping\Id;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(ArticleRepository $repo)
    {
        return $this->render('home/index.html.twig', [
            "articles" => $repo->findAll(),
        ]);
    }

    /**
     * @Route("/add-article", name="add")
     */
    public function add(Request $request, ObjectManager $object) {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $object->persist($article);
            $object->flush();
        }

        return $this->render("article/index.html.twig", [
            "form" => $form->createView(),
            "article" => $article
        ]);
    }

    /**
     * @Route("/article/{id}", name="find_article")   
     */
    public function find(int $id, ArticleRepository $repo) {
        return $this->render("home/find-article.html.twig", [
            "article" => $repo->find($id),
        ]);
    }
}