<?php

namespace App\Form;

use App\Entity\Article;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => "Nom du tableau:",
                'attr' => ['class' => 'articleName']
            ])
            ->add('artist', TextType::class, [
                'label' => "Le peintre:",
                'attr' => ['class' => 'articleArtist']
            ])
            ->add('text', TextareaType::class, [
                'label' => "Description du tableau:",
                'attr' => ['class' => 'articleText']
            ])
            ->add('imageArticle', TextType::class, [
                'label' => "URL du tableau:",
                'attr' => ['class' => 'articleImage']
            ])
            ->add('image', TextType::class, [
                'label' => "URL de prévisualisation du tableau:",
                'attr' => ['class' => 'articleImagePreview']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
        ]);
    }
}
