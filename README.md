# Blog PHP

### Description

Version 1.0.0

Le but de ce projet est de créer un blog simple avec la possibilité d’ajouter de nouveaux articles, de les modifier ou de les supprimer.

------

### Etat actuel du projet

Sur la page d'accueil du blog, il y a un carrousel avec un aperçu des images réduites des peintures. Sur chaque image, il y a un lien pour aller aux pages du tableau particulièr.
Également sur la page principale, il y a un bouton "Ajouter un nouvel article", qui envoie l'utilisateur à la page de création d'un nouveau post avec la description du tableau.



#### Voici un code en PHP:

```
    /**
     * @Route("/add-article", name="add")
     */
    public function add(Request $request, ObjectManager $object) {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $object->persist($article);
            $object->flush();
        }

        return $this->render("article/index.html.twig", [
            "form" => $form->createView(),
            "article" => $article
        ]);
    }
```

------

### **En version 2.0.0**

À l'avenir, je prévois d'ajouter la possibilité d'écrire des commentaires sur les articles avec une description des tableaux. Bien sûr, les commentaires peuvent également être corrigés ou supprimés.

Également il sera ajouté la possibilité d'enregistrer des utilisateurs.

------

### **L'image du jeu**

![main](https://gitlab.com/Avrora/projet-blog/raw/master/public/Style/main.jpg)



![ajoutez](https://gitlab.com/Avrora/projet-blog/raw/master/public/Style/Ajoutez.png)

### **Fabriqué par**

- PHP
- CSS
- Symfony
- MariaDB
- Bootstrap